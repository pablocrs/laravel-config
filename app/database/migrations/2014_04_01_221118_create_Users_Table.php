<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    /// crear bdd:  php artisan migrate
	public function up()
	{
        $table->increments('id');

        $table->string('name')->unique();
        $table->string('email')->unique();
        $table->string('password');

        $table->string('type');

        $table->softDeletes();
        $table->timestamps();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('users');
	}

}
